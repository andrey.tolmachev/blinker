#Makefile
build:
	./gradlew build && docker build --build-arg JAR_FILE=build/libs/demo-0.0.1-SNAPSHOT.jar . -t atol/blinker
	
test: build
	./gradlew build integrationTest

deploy:
	ssh demo@159.65.198.5 'bash -s' < deploy.sh

start: build
	./deploy.sh

#Ignore subfolders with this names
.PHONY: build deploy