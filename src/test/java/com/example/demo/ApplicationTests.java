package com.example.demo;

import com.example.demo.model.Link;
import com.github.database.rider.core.DBUnitRule;
import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.api.dataset.DataSet;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class }, webEnvironment
		= SpringBootTest.WebEnvironment.DEFINED_PORT)
@DBUnit(url = "jdbc:h2:mem:bootapp;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver", user = "sa")
public class ApplicationTests {
	private static final String API_ROOT
			= "http://localhost:8080/api";

	@Rule
	public DBUnitRule dbUnitRule = DBUnitRule.instance();

	@Test
    @DataSet(value = "datasets/links.yml")
	public void shouldReturnAvailableLinks() {
        Link[] expected = new Link[] {new Link(1l, "https://martinfowler.com/articles/mocksArentStubs.html")};

        Response response = RestAssured.get(API_ROOT + "/links");

		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertArrayEquals(response.getBody().as(Link[].class), expected);
	}
}


