package com.example.demo;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;

import static org.junit.Assert.assertEquals;

public class ContainerTest {
    @Rule
    public GenericContainer container
            = new GenericContainer("atol/blinker")
            .withExposedPorts(8080);

    @Test
    public void shouldReturnHelloWorld_whenGetHello() {
        String address = "http://"
                + container.getContainerIpAddress()
                + ":" + container.getMappedPort(8080)
                + "/hello";
        Response response = RestAssured.get(address);

        assertEquals("Hello, World!", response.getBody().asString());
    }
}
