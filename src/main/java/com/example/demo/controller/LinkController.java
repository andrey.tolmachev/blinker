package com.example.demo.controller;

import com.example.demo.model.Link;
import com.example.demo.provider.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LinkController {

    @Autowired
    LinkRepository linkRepository;

    @RequestMapping("/api/links")
    public List<Link> getLinks() {
        return linkRepository.findAll();
    }
}
