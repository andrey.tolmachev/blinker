package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableJpaRepositories("com.example.demo.provider")
@EntityScan("com.example.demo.model")
@RestController
public class Application {

    @RequestMapping("/hello")
    public String sayHello() {
        return "Hello, World!";
    }


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}

