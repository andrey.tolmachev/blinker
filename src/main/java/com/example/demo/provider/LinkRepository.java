package com.example.demo.provider;

import com.example.demo.model.Link;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LinkRepository extends CrudRepository<Link, Long> {
    List<Link> findAll();
}

